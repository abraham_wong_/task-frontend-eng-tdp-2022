
import 'assets/css/style.css';
import 'assets/css/custom.css';
import Navbar from 'assets/components/Navbar';
import Footer from 'assets/components/Footer';
import { Navigate, Route, Routes } from 'react-router-dom';
import Home from 'assets/page/Home';
import Shop from 'assets/page/Shop';
import ShopDetail from 'assets/page/ShopDetail';
import Login from 'assets/page/Login';
import Profile from 'assets/page/Profile';

const RequireAuth = (({ children }) => {
    const isLogin = localStorage.getItem("user_info");
    return isLogin ? children : <Navigate to={"/login"} />;
});

const AuthSuccess = (({ children }) => {
    const isLogin = localStorage.getItem("user_info");
    return isLogin ? <Navigate to="/" /> : children;
})

const App = () => {
    return (
        <>
            <Navbar />

            <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/shop' element={<Shop />} />
                <Route path='/shop/:productId' element={<ShopDetail />} />
                <Route path='/login' element={
                    <AuthSuccess>
                        <Login />
                    </AuthSuccess>
                } />
                <Route path='/profile' element={
                    <RequireAuth>
                        <Profile />
                    </RequireAuth>
                } />
                <Route path='*' element={<Navigate to="/" />} />
            </Routes>
            <Footer />
        </>
    );
}

export default App;
