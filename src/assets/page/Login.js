import LoginForm from 'assets/components/LoginForm'
import React from 'react'

function Login() {
  return (
    <div className='content padding' style={{ maxWidth: '1564px' }}>
        <LoginForm />
    </div>
  )
}

export default Login