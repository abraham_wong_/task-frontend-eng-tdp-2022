import Banner from 'assets/components/Banner'
import Category from 'assets/components/CategoryMap'
import React from 'react'

function Home() {
    return (
        <>
            <Banner />
            <div className='content padding' style={{ maxWidth: '1564px' }}>
                <Category />
            </div>
        </>
    )
}

export default Home