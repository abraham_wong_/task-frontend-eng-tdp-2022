import ProfileForm from 'assets/components/ProfileForm'
import React from 'react'

function Profile() {
  return (
    <div className='content padding' style={{ maxWidth: '1564px' }}>
        <ProfileForm />
    </div>
  )
}

export default Profile