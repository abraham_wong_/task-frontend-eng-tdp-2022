import ProductDetail from 'assets/components/ProductDetail'
import React from 'react'

function ShopDetail() {
    return (
        <div className='content padding' style={{ maxWidth: '1564px' }}>
            <ProductDetail />
        </div>
    )
}

export default ShopDetail