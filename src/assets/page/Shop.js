import Products from 'assets/components/Products'
import React from 'react'

function Shop() {
  return (
    <div className='content padding' style={{ maxWidth: '1564px' }}>
        <Products />
    </div>
  )
}

export default Shop