function useRupiahFormatter() {
    const rupiahFormat = new Intl.NumberFormat('id', {
        style: 'currency',
        currency: 'IDR'
    });

    return rupiahFormat;
}

export default useRupiahFormatter