import axios from 'axios';
import { useEffect, useState } from 'react'

axios.defaults.baseURL = "https://api-tdp-2022.vercel.app/api"

function useAxios(url, method, request) {
    const [code, setCode] = useState(0);
    const [message, setMessage] = useState(null);
    const [response, setResponse] = useState(null)
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        fetchData(request)
    }, [request]);

    const fetchData = (request) => {
        setIsLoading(true);
        setCode(0);
        if (method !== "get" && request && Object.keys(request).length === 0) return;

        axios({
            method: method,
            url: url,
            data: request
        }).then((response) => {
            setCode(response?.data?.code);
            setResponse(response?.data?.data);
            setMessage(response?.data?.message);
        }).catch(err => {
            setCode(err.response.data.code)
            setMessage(err.response.data.message);
        }).then(_ => {
            setIsLoading(false);
        })
    }

    return { code, message, response, isLoading, fetchData };
}

export default useAxios