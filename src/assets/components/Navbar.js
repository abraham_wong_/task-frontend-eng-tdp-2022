import React from 'react'
import { Link } from 'react-router-dom'

function Navbar() {

    const handleLogout = e => {
        localStorage.clear();
        window.location.reload();
    }

    return (
        <div className="top" >
            <div className="bar white wide padding card">
                <Link to="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</Link>
                <div className="right hide-small">
                    <Link to="/" className="bar-item button">Home</Link>
                    <Link to="/shop" className="bar-item button">Shop</Link>

                    {
                        localStorage.getItem("user_info") ?
                            <>
                                <Link to="/profile" className="bar-item button">My Profile</Link>
                                <a href="/" className="bar-item button" onClick={handleLogout}>Logout</a>
                            </>
                            :
                            <>
                                <Link to="/login" className="bar-item button">Login</Link>
                            </>
                    }
                </div>
            </div>
        </div>
    )
}

export default Navbar