import useAxios from 'assets/hook/useAxios'
import useRupiahFormatter from 'assets/hook/useRupiahFormatter';
import React, { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import Loading from './Loading';

function ProductDetail() {
    const params = useParams();
    const { response, isLoading } = useAxios(`/products/${params?.productId}`)

    const [quantity, setQuantity] = useState(0);

    const navigate = useNavigate();

    const rupiahFormat = useRupiahFormatter();

    const onChangeQuantity = e => {
        const value = e.target.value;

        if (value.trim() === '')
            setQuantity(0);
        else if (value < response?.stock)
            setQuantity(value);
        else
            setQuantity(response?.stock);
    }

    const onSubmitPurchase = e => {
        e.preventDefault();
        const isLogin = localStorage.getItem("user_info");
        
        if (isLogin) {
            alert("Product added to cart successfully");
            window.location.replace("/");
        } else {
            navigate("/login");
        }
    }

    return (
        <>
            <div className="container padding-32" id="about">
                <h3 className="border-bottom border-light-grey padding-16">Product Information</h3>
                {
                    isLoading ? <Loading /> :
                        <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
                            <div className="col l3 m6 margin-bottom">
                                <div className="product-tumb">
                                    <img src={response?.image} alt={response?.title} />
                                </div>
                            </div>
                            <div className="col m6 margin-bottom">
                                <h3>{response?.title}</h3>
                                <div style={{ marginBottom: '32px' }}>
                                    <span>Category : <strong>{response?.category}</strong></span>
                                    <span style={{ marginLeft: '30px' }}>Review : <strong>{response?.rate}</strong></span>
                                    <span style={{ marginLeft: '30px' }}>Stock : <strong>{response?.stock}</strong></span>
                                    <span style={{ marginLeft: '30px' }}>Discount : <strong>{response?.discount} %</strong></span>
                                </div>
                                <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                                    {rupiahFormat.format(response?.price)}
                                </div>
                                <div style={{ marginBottom: '32px' }}>
                                    {response?.description}
                                </div>
                                <div style={{ marginBottom: '32px' }}>
                                    <div><strong>Quantity : </strong></div>
                                    <input type="number" className="input section border" name="total" placeholder='Quantity'
                                        min={0} max={response?.stock}
                                        value={quantity} onChange={onChangeQuantity} />
                                </div>
                                <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                                    Sub Total : {rupiahFormat.format(response?.price * quantity * (1 - (response?.discount / 100)))}
                                    {
                                        response?.discount > 0 ?
                                            <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}>
                                                <strong>{rupiahFormat.format(response?.price * quantity)}</strong>
                                            </span> : ""
                                    }
                                    { }
                                </div>
                                {
                                    response?.stock > 0 ?
                                        <button className='button light-grey block' disabled={(quantity == 0)} 
                                            onClick={onSubmitPurchase}>Add to cart</button> :

                                        <button className='button light-grey block' disabled={true}>Empty Stock</button>
                                }
                            </div>
                        </div>
                }
            </div>
        </>
    )
}

export default ProductDetail