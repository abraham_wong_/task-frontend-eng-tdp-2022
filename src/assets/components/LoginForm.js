import useAxios from 'assets/hook/useAxios';
import React, { useEffect, useState } from 'react'

function LoginForm() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [request, setRequest] = useState({});
    const [login, setLogin] = useState(false)

    let { code, message, isLoading, response, fetchData } = useAxios("/login", "post", request);

    const onLogin = e => {
        e.preventDefault();
        setLogin(true);

        setRequest({
            username: username,
            password: password
        });
    }

    useEffect(() => {
      fetchData(request);
    }, [request])
    
    useEffect(() => {
        if (code === 200) {
            localStorage.setItem("user_info", `{"username": "${username}"}`);
            localStorage.setItem("access_token", `${response.access_token}`);
            window.location.replace("/");
        } else if (login && code === 400) {
            alert(message);
            setLogin(false);
        }
    }, [isLoading]);

    return (
        <div className="container padding-32" id="contact" >
            <h3 className="border-bottom border-light-grey padding-16">Login</h3>
            <p>Lets get in touch and talk about your next project.</p>
            <form onSubmit={onLogin}>
                <input className="input border" type="text" placeholder="Username" required name="username"
                    value={username} onChange={e => setUsername(e.target.value)} />
                <input className="input section border" type="password" placeholder="Password" required name="Password"
                    value={password} onChange={e => setPassword(e.target.value)} />
                <button className="button black section" type="submit"
                    disabled={login}>
                    <i className="fa fa-paper-plane" />
                    {login ? "Loading..." : "Login"}
                </button>
            </form>
        </div >
    )
}

export default LoginForm