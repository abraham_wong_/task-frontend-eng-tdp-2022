import useAxios from 'assets/hook/useAxios'
import React from 'react'
import { useSearchParams } from 'react-router-dom';
import Loading from './Loading';
import { Link } from 'react-router-dom'
import useRupiahFormatter from 'assets/hook/useRupiahFormatter';

function Products() {
    const [searchParams, setSearchParams] = useSearchParams();
    const categoryId = searchParams.get("category");
    const { response, isLoading } = useAxios
        (
            categoryId == null ?
                "/products" : `/products?category=${categoryId}`
        );

    const rupiahFormat = useRupiahFormatter();

    return (
        <>
            <div className="container padding-32" id="about">
                <h3 className="border-bottom border-light-grey padding-16">
                    {categoryId == null ?
                        "All Product" :
                        `Product Category ${response?.categoryDetail?.name ?? ""}`}
                </h3>
            </div>
            {
                isLoading ? <Loading /> :
                    <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                        {
                            response?.productList?.map((product, index) => {
                                return (
                                    <div className="product-card" key={index}>
                                        {product.discount > 0 ? <div className="badge">Discount</div> : " "}
                                        <div className="product-tumb">
                                            <img src={product.image} alt={product.title} />
                                        </div>
                                        <div className="product-details">
                                            <span className="product-catagory">{product.category}</span>
                                            <h4>
                                                <Link to={`/shop/${product.id}`}>
                                                    {product.title}
                                                </Link>
                                            </h4>
                                            <p>{product.description}</p>
                                            <div className="product-bottom-details">
                                                <div className="product-price">{rupiahFormat.format(product.price)}</div>
                                                <div className="product-links">
                                                    <Link to={`/shop/${product.id}`}>View Detail<i className="fa fa-heart"></i></Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
            }
        </>
    )
}

export default Products