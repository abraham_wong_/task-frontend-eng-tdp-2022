import useAxios from 'assets/hook/useAxios'
import React, { useEffect, useState } from 'react'
import Loading from './Loading';

function ProfileForm() {
    const userInfo = JSON.parse(localStorage.getItem("user_info"));
    
    const { response: getResponse, isLoading: getLoading } = useAxios(`/profile/${userInfo.username}`);
    
    const [requestBody, setRequestBody] = useState({})
    const [update, setUpdate] = useState(false)

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");

    const [validationName, setValidationName] = useState("");
    const [validationEmail, setValidationEmail] = useState("");
    const [validationPhoneNumber, setValidationPhoneNumber] = useState("");
    const [validationPassword, setValidationPassword] = useState("");
    const [validationConfirm, setValidationConfirm] = useState("");

    const onSubmitProfileUpdate = e => {
        e.preventDefault();

        let errors = validate();
        if (Object.keys(errors).length > 0) {
            setValidationName(errors?.name);
            setValidationEmail(errors?.email);
            setValidationPhoneNumber(errors?.phoneNumber);
            setValidationPassword(errors?.password);
            setValidationConfirm(errors?.confirmPassword);
            return;
        }

        setUpdate(true);
        setValidationName("");
        setValidationEmail("");
        setValidationPhoneNumber("");
        setValidationPassword("");
        setValidationConfirm("");

        setRequestBody({
            name: name,
            email: email,
            phoneNumber: phoneNumber,
            password: password
        });
    }

    const { code, message, isLoading: putLoading, fetchData } = useAxios(`/profile/${name}`, "put", requestBody);

    useEffect(() => {
        setName(getResponse?.username);
        setEmail(getResponse?.email);
        setPhoneNumber(getResponse?.phoneNumber);
    }, [getLoading])
    
    useEffect(() => {
        fetchData(requestBody);
    }, [requestBody])

    useEffect(() => {
        if (code === 200 || code === 400) {
            setUpdate(false);
            alert(message);
            window.location.reload();
        }
    }, [putLoading])

    const validate = () => {
        let errors = {};
        if (name.trim() === "")
            errors.name = "Name is required"

        if (email.trim() === "")
            errors.email = "Email is required"
        else if (email.split("").filter((x) => x === "@").length !== 1
            || email.indexOf(".") === -1)
            errors.email = "Email is invalid"

        if (phoneNumber.trim() === "")
            errors.phoneNumber = "Phone number is required"
        else if (isNaN(phoneNumber))
            errors.phoneNumber = "Phone number invalid"
        else if (phoneNumber.length !== 12)
            errors.phoneNumber = "Phone number invalid"

        if (password.trim() === "")
            errors.password = "Password is required"

        if (passwordConfirm.trim() === "")
            errors.confirmPassword = "Retype password is required"
        else if (password !== passwordConfirm)
            errors.confirmPassword = "Retype password invalid"

        return errors;
    }

    return (
        <div className="container padding-32" id="contact" >
            {
                getLoading ? <Loading /> :
                    <>
                        <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
                        <p>Lets get in touch and talk about your next project.</p>
                        <form onSubmit={onSubmitProfileUpdate}>
                            <input className="input section border" type="text" placeholder="Name" name="name"
                                value={name} onChange={e => setName(e.target.value)} />
                            <div style={{ color: '#EF144A', display: validationName === "" ? 'none' : 'block' }}>
                                {validationName}
                            </div>

                            <input className="input section border" type="text" placeholder="Email" name="email"
                                value={email} onChange={e => setEmail(e.target.value)} />
                            <div style={{ color: '#EF144A', display: validationEmail === "" ? 'none' : 'block' }}>
                                {validationEmail}
                            </div>

                            <input className="input section border" type="text" placeholder="Phone Number" name="phoneNumber" maxLength={12}
                                value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)} />
                            <div style={{ color: '#EF144A', display: validationPhoneNumber === "" ? 'none' : 'block' }}>
                                {validationPhoneNumber}
                            </div>

                            <input className="input section border" type="password" placeholder="Password" name="password"
                                value={password} onChange={e => setPassword(e.target.value)} />
                            <div style={{ color: '#EF144A', display: validationPassword === "" ? 'none' : 'block' }}>
                                {validationPassword}
                            </div>

                            <input className="input section border" type="password" placeholder="Retype Password" name="retypePassword"
                                value={passwordConfirm} onChange={e => setPasswordConfirm(e.target.value)} />
                            <div style={{ color: '#EF144A', display: validationConfirm === "" ? 'none' : 'block' }}>
                                {validationConfirm}
                            </div>

                            <button className="button black section" type="submit" disabled={update}>
                                <i className="fa fa-paper-plane" /> { update ? "Loading..." : "Update"}
                            </button>
                        </form>
                    </>
            }
        </div>
    )
}

export default ProfileForm