import useAxios from 'assets/hook/useAxios';
import React from 'react'
import map from '../../assets/img/map.jpg';
import Loading from './Loading';
import { Link } from 'react-router-dom'

function Category() {
    const { response, isLoading } = useAxios("/categories");

    return (
        <>
            {isLoading ? <Loading /> : <>
                <div className="container padding-32" id="projects">
                    <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
                </div>
                <div className="row-padding">
                    {
                        response?.map((category, index) => {
                            return (
                                <div className="col l3 m6 margin-bottom" key={index}>
                                    <Link to={`/shop?category=${category?.id}`}>
                                        <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                                            <div className="display-topleft black padding">{category?.name}</div>
                                            <img src={category.image} alt={category.name} style={{ maxWidth: '100%', minHeight: '100%' }} />
                                        </div>
                                    </Link>
                                </div>
                            )
                        })
                    }
                </div>

                <div className="container padding-32">
                    <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
                </div>
            </>}
        </>
    )
}

export default Category